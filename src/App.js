import './App.css'
import Stories from 'react-insta-stories'
import img_background from './images/img_background.jpg'
import { hints, imgs } from './components/config'
import vid_1 from './videos/vid_1.mp4'
import vid_2 from './videos/vid_2.mp4'
import vid_3 from './videos/vid_3.mp4'
import vid_4 from './videos/vid_4.mp4'

let storiesArray = []
const videosArray = [
  { url: vid_1, type: 'video', duration: 8000 },
  { url: vid_2, type: 'video', duration: 12000 },
  { url: vid_3, type: 'video', duration: 8000 },
  { url: vid_4, type: 'video', duration: 15000 },
]

const contentStory = (img, text) => (
  <div style={{ background: 'pink', padding: 20 }}>
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        margin: '2px 2px',
      }}
    >
      <img src={img} alt="img_2" Height={400} style={{ maxWidth: '560px' }} />
    </div>

    <h4 style={{ marginTop: '20px' }}>{text}</h4>
  </div>
)

const backgroundStyle = {
  backgroundImage: 'url(' + img_background + ')',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
  backgroundPosition: 'center center',
  height: '100vh',
}
const storyContainerStyles = {
  marginLeft: 'auto',
  marginRight: 'auto',
  height: '100vh',
  background: 'pink',
}

function App() {
  hints.map((hint, index) => {
    storiesArray.push({
      content: () => contentStory(imgs[index] ? imgs[index] : imgs[0], hint),
    })
  })

  storiesArray = [...storiesArray, ...videosArray]
  return (
    <div style={backgroundStyle}>
      <Stories
        stories={storiesArray}
        defaultInterval={30000}
        width={600}
        height="100vh"
        storyContainerStyles={storyContainerStyles}
        keyboardNavigation={true}
      />
    </div>
  )
}

export default App
