import img_0 from '../images/img_0.jpg'
import img_1 from '../images/img_1.jpg'
import img_2 from '../images/img_2.jpg'
import img_3 from '../images/img_3.jpg'
import img_4 from '../images/img_4.jpg'
import img_5 from '../images/img_5.jpg'
import img_6 from '../images/img_6.jpg'
import img_7 from '../images/img_7.jpg'
import img_8 from '../images/img_8.jpg'
import img_9 from '../images/img_9.jpg'
import img_10 from '../images/img_10.jpg'
import img_11 from '../images/img_11.jpg'
import img_12 from '../images/img_12.jpg'
import img_13 from '../images/img_13.jpg'
import img_14 from '../images/img_14.jpg'
import img_15 from '../images/img_15.jpg'
import img_16 from '../images/img_16.jpg'
import img_17 from '../images/img_17.jpeg'
import img_18 from '../images/img_18.jpeg'
import img_19 from '../images/img_19.JPG'
import img_20 from '../images/img_20.JPG'
import img_21 from '../images/img_21.JPG'
import img_22 from '../images/img_22.JPG'
import img_23 from '../images/img_23.JPG'
import img_24 from '../images/img_24.JPG'
import img_25 from '../images/img_25.JPG'
import img_26 from '../images/img_26.JPG'
import img_27 from '../images/img_27.JPG'
import img_28 from '../images/img_28.JPG'
import img_29 from '../images/img_29.jpg'
import img_30 from '../images/img_30.jpg'
import img_31 from '../images/img_31.jpg'
import img_32 from '../images/img_32.jpg'
import img_33 from '../images/img_33.jpg'
import img_34 from '../images/img_34.jpg'
import vid_1 from '../videos/vid_1.mp4'
import vid_2 from '../videos/vid_2.mp4'
import vid_3 from '../videos/vid_3.mp4'
import vid_4 from '../videos/vid_4.mp4'
const hints = [
  'Maurice: Querido Chávez, ahora que te veo partir, lamento haberme burlado tanto de tu poca o nula capacidad para hacer dibujos en pictionary. Espero que tu partida no haya tenido que ver con ese penoso incidente. Que tu nuevo equipo sepa que la mitad de tu corazón está acá y la otra mitad en la av. Santa Rosa Chincha.',
  'Mike: Estimado Sergio, siempre recordaremos tu perseverancia con el nombre sisoli, aun recuerdo cuando lo mencionabas y todos nos reiamos por lo gracioso que sonaba, debes admitir que Facilita suena mas chevere. Tus dotes para el canto las vimos en el ñañofest y pues, no cabe duda que la programación es lo tuyo. Los mejores exitos a donde sea que vayas, y recuerda, que si no se rien de tus chistes, no son ellos, eres tu, pero la gente va a hablar tu sigue nomas, L-gante que lo que!',
  'Eli: Amigo, es una pena no haber podido probar nunca tu famosa carapulcra. Espero que no estafes a tus nuevos compañeros de trabajo con tus habilidades culinarias. Muchos éxitos y un fuerte abrazo!',
  'Ñaño: Amigo Tekio, pucha ya como 6 años trabajando juntos en distintos trabajos, ya mucho ya. Nada amigo espero que ahora en tu nueva chamba sepan apreciar tu sarcasmo, sentido del humor y tu forma de hablar en borrador. Espero ellos sepan siempre de que hablas y no como yo que te hacia mil preguntas para entenderte. Por siempre Vamos pa chincha familia, chincha cuna de campeones.',
  'Camola: José Chávez, espero que en tu nuevo trabajo sí se rían de todos tus chistes, que canten las canciones que piden en el karaoke para que no te sientas obligado a cantar, que les guste bizarrap, que caigan en todas tus mentiras, no se burlen de tus dibujos y que no se aburran con tus explicaciones. Lo mejor para ti, siempre! ',
  'Silvanator: Querida mascotita: Mientras escribo esto empiezo a recordar todo lo que hemos pasado contigo en esto últimos 4 años ¡qué bárbaro! ¿Ya mucho tiempo, no? Ya era hora de que lleves tus chistes malos a otros lares. Seguro encuentras a alguien que se ría de ti cuando los cuentas. Lideraste la plataforma única de solicitudes digitales del Estado peruano pero no pudiste completar bien la solicitud para el Cochinola, jamás te lo perdonaremos. Quién sabe si habrá otro. Fuera de bromas, se te extrañará en los almuerzos ¿ahora, a quién le voy a contar mis dramas? Te extrañaré mucho en las reuniones técnicas con viejos lesbianos. Gracias por tu apoyo todo este tiempo. Sorry por las discusiones. Que te vaya súper en tu nueva chamba y que te aguanten como lo hemos hecho nosotros. Abrazos, Sil.',
  'Mascota: Habla, pe, mascota! Me da mucha pena que nos dejes sin haber aprendido muchas cosas como... dejar de hablar en borrador, apreciar los buenos postres y el chocolate, conseguir la entrada correcta para Cochinola, en fin... Tienes mucho camino por delante, Ómicron, y me sentiré orgullosa cuando vea que aprendiste al menos una de esas cosas. Que te vaya super en tu próxima chamba y que algún ser divino proteja de ti a tus pobres nuevos compañeros. Un abrazo!',
  'Fiore: Un gusto haberte conocido y trabajado en el mismo equipo. Estoy segura que te va a ir súper en todo lo nuevo que se viene! Te deseo siempre lo mejor! Un abrazo virtual. ',
  'Yan: José maestro de maestros me acabo de enterar que cambias de rumbo y de verdad me apena que ya dejes el equipo, pero se que el nuevo rumbo que tomas será por un nuevo reto profesional, lo mayores y mejores éxitos, espero que aún siga pendiente la famosa carapulcra, !!!',
  'Draculín: José hermano, con total sorpresa por esta noticia pero feliz porqué será una nueva etapa de crecimiento y trabajo en equipo ( No lo olvides XD) espero que te lleves un poco de bushido para que la fuerza te acompañe  siempre XD. Un gustazo haber trabajado contigo capo dejas mucho en todo el team eres de los pilares que construyeron todo lo que es ahora gob.pe y eso tiene un valor para la historia. Un gran abrazo y gracias por toda esa buena onda y gran persona que eres hermano. ¡éxitos totales!',
  'Kaori: José de desarrollo, y ahora… ¿quién me va a ayudar a detectar errores estúpidos en mis guías? Te deseo muchos éxitos en tu nuevo camino como falso chef de carapulcras. Espero que tus comensales imaginarios disfruten de tus exquisitos platos ficticios. ¡Un abrazo!',
  'Moi: José espero que en tu próximo trabajo te aprecien tanto como aquí y te dejen acaparar también el microfono  en los Karaokes. Nunca olvidaré como me apoyabas en los Sprints, me subías el ánimo y eso vale más que mil carapulcras :_D. Ya nos juntaremos en la cancha o pa jugar Switch o en el próximo Ñaño Fest pq siempre serás del Grupo.  Un enorme abrazo.',
  'Mauro: Amigo, muchos éxitos para ti en tu nuevo destino!, un gustazo conocerte, eres un gran profesional y una gran persona. Dejas un gran vacío en el equipo, se va a extrañar tus ocurrencias y buen humor XD.',
  'Mitsu: Amigooo se te va a extrañar en el equipo de Facilita, por tu buena chamba pero sobre todo por tu famosa y galardonada carapulcra. Éxitos, que te vaya de ptm en lo que venga! ',
  'Marieth: Jose amigo, gracias por tanto por la paciencia en todo y en especial en los los pair programming jajajaja …  he aprendido un montón de ti dejas muchas enseñanzas para el equipo… recuerdo siempre la perseverancia que tenias en sisoli y lo mencionabas muchas veces pasado un tiempo funciono :)  … te extrañaremos un monton se que en el nuevo trabajo donde iras te ira super bien porque le pones mucha pasion en todo lo que haces ...mis mejores deseos para ti amigo …un gran abrazo :) ',
  'Neytor: Amigo un gustaso haberte conocido en persona, eres una persona super genial, te deseo el mejor de los exitos en los nuevos horizontes que te esperan, sin duda alguna se te extrañara, dicen que tus chistes eran malos, pero a mi me hacian reir, o quizas esos no eran chistes xd, super exitos amigo, metele ganas a todo como siempre, cuidate y un abrazo virtual <3',
  'Pau: Alas y buen viento y todos mis buenos deseos, etc etc, pero: ¿quién me va a resolver las dudas existenciales sobre las guías? Pero lo bueno es que ahora ya nos podemos ignorar para siempre... Fuera de bromas (nos dijeron que no nos pongamos dramáticos), mis mejores deseos para tus nuevos proyectos, por tus logros con Gob.pe y (Sisoli) Facilita, sabemos que llegarás lejos.',
  'Perruns: Todos los éxitos de la vida, espero que te vaya de pm donde vayas. ',
  'Rai: José, nuevamente agradecerte por la oportunidad brindada para integrarme al equipo. Eres una excelente persona y profesional, estoy seguro que en tu nuevo reto te ira muy bien porque eres un crack en lo que haces. Te deseo los mejores exitos. Un fuerte abrazo de Gol hermano. ',
  'Renzo: José, ¿cómo que te vas sin haber invitado tu célebre carapulcra?, se va a extrañar tu buena vibra y tus particulares ocurrencias. Gracias por todo el apoyo en estos meses, eres un crack y fijo la vas a seguir rompiendo en donde estés. Un abrazo y no te burles tanto cuando recuerdes lo del gitlab y la tarjeta de crédito jajaj.',
  'Paul: Amigo Jose, agradecerte siempre por la empatia y amabilidad con la que despejabas mis dudas. Que el senor derrame muchas bendiciones en tu caminar. Un fuerte abrazo a la distancia, y ya habra despedida',
  'Juan Carlos: José, ante todo agradecerte por la oportunidad que junto a Diego me brindaron de integrarme al equipo, en el poco tiempo he notado que eres un crack en tu trabajo y por eso sé que te irá muy bien en tu nueva chamba, un fuerte abrazo desde Piura!',
  'Thalia: José Sergio(un jefe distinto xD) voy a extrañarte un montón, no a todos les preguntas un montón de cosas y te responden con un jajaja sin leer siquiera lo que preguntaste(excusa estaba en reunión), gracias por todo este tiempo de apoyo tanto en el tema técnico como en trámites, eres un gran líder y un capo en tecnología, te deseo lo mejor del mundo, que sigas brillando a donde sea que vayas :)',
  'Venus: Ahora que te vas ya no habrá alguien que exprese su odio por cada persona que dice que le gusta Harry Potter en un meet. Creo que era el destino. Aunque no entiendo porque te vas, las « reuniones por lineamientos técnicos » de Sunat fueron demasiado, no? Si, tal vez fue eso. Espero que en tu nuevo trabajo sean cheveres cuando te pidan cosas, asi como nosotros, haciendote quedar hasta las 3 a. m. porque se cayó Pase laboral, Bonos, Vacunas, Yanapay, etc. Respecto a tus chistes, nunca escuché alguno tuyo, y si lo hiciste y no me di cuenta, ahora entiendo xq te va mejor como programador. Te recordaré con mucho cariño, a veces.',
  'Chelo: Jooose!!! Bueno, de la chamba no puedo decir mucho… aunque por todos los comentarios pareces chevere jaja… Solo puedo asegurar que fuimos el mejor equipo de Chincha, aunque hayamos perdido con trampa, tenías todas las respuestas y eso es de capos jaja! Espero nos volvamos a ver para otro reencuentro/despedida y que tu local siga disponible para todos nosotros jajaja! Un gran abrazo y ÉXITOS EN TODO!!!',
  'Suny: José, miles de éxitos en tu camino profesional! Qué pena que no pudimos probar tu platillo bandera: la famosísima carapulcra. Espero que puedas deleitar con tus tan aclamadas recetas a tus nuevos compañeres. Gracias por la chambaza en Gob.pe (lo digo como ciudadana). Abrazo!',
  'Alme: ¿Almendra? ¿De qué entidad? jajaja estimado Jose, siempre me voy a acordar de la primera vez que te escribí por un issue y me respondiste como solo tú sabes! Es triste que te vayas profesionalmente de este equipo pero tu legado y todos los buenos recuerdos quedan. Deseo que te vaya genial en todo lo que hagas! Muchos éxitos y alegrías para ti. Gracias por darnos tu local en Chincha, fue un día hermoso! Espero que se repita. En mi tienes a una amiga y cuando quieras ir a Tarapoto me avisas :) un abrazo!',
  'Bethania: Otra vez tú? Buenas vibras en tu nueva chamba. Ojalá valoren tu talento de animador de eventos corporativos.',
  'Carlos A: Papu, por que!!!! Y ahora ya no funcionará el chiste del santo patron de los Devs. ;( En fin, me la he estado pasando muy bien. Y he aprendido un montón. He dejado de ser tan llorica respecto a codear y he progresado con las cripticas recomendaciones que pasabas. XD Espero que donde vayas a parar te diviertas un montón, puedas seguir codeando y aprendiendo. O te la juegues con algún emprendimiento. Pero haciendo hincapié, espero que te diviertas un montón.',
  'Ale: José, muchos éxitos en tus nuevos proyectos profesionales. Estoy segura que te irá genial. Si bien no tuvimos muchas oportunidades para trabajar juntos, recuerdo que alguna vez sí te fastidié con issues jaja. ¡Un abrazote!',
  'Daniela: José Sergio, OE, cómo que te vas! Espero que en tu nueva chamba la pases tan bien como cuando hicimos el triaje digital xD No, mentira, que te vaya suuuper bien!! Espero que a donde vayas sí entiendan tus dibujitos de pictionary y que te pongan BTS todo el día. Éxitos en todo amix!! ',
  'Ronald: José. quizas no hemos compartido mucho durante el trabajo, salvo cuando te molestaba para que revises mi informe xD Pero en lo poco que hemos interactuado me pude dar cuenta de tu liderazgo y forma de ser, ademas el hecho de apreciar el vacio que los demas expresan sobre tu partida da muy buena vibra sobre ti, te deseo lo mejor de los éxitos en lo que te hayas propuesto realizar y gracias por la confianza y el apoyo ;)',
  'Manuel: José, espero que te vaya muy bien en tus nuevos retos, la veces que tratamos se agradece la amabilidad, empatía y lo practico que fuiste al dar soluciones. También las gracias por la confianza, los consejos y las oportunidades de liderar dev en los proyectos. Aprendi mucho de ti, muchas gracias. ',
  'Gino: Si Messi se fue del Barcelona... bueno estimado cuidate a dónde vayas!',
  'David: Estimado Jose!, se dice que por mejoria, mi casa dejaria y en esta oportunidad toca despedirnos, solo es un hasta luego amigo, siempre seras bienvenido. Eres una buena persona y buen profesional, un gusto haber compartido contigo este tiempo, amical y laboralmente. Te deseo lo mejor, seguro que nos volveremos a encontrar. Un abrazo y exitos!'
]
const imgs = [
  img_1,
  img_2,
  img_3,
  img_4,
  img_5,
  img_6,
  img_7,
  img_8,
  img_9,
  img_10,
  img_11,
  img_12,
  img_13,
  img_14,
  img_15,
  img_16,
  img_17,
  img_18,
  img_19,
  img_20,
  img_21,
  img_22,
  img_23,
  img_24,
  img_25,
  img_26,
  img_27,
  img_28,
  img_29,
  img_30,
  img_31,
  img_32,
  img_33,
  img_34,
  img_0,
  vid_1,
  vid_2,
  vid_3,
  vid_4,
]
export { hints, imgs }
